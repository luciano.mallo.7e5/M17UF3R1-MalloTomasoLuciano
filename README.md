# M17 - UF3 - R1 - Mallo Tomaso Luciano Ezequiel

## To play with the fixed carmera:

- It's needs to charge the Test-R1.1.1 scene.
- Here the player got  some obtacles to jump an interact to test the movement of the avatar.
- The movement can be done with the button [W,A,S,D] and the arrows buttons[UP,DOWN,LEFT,RIGHT].
- To jump the [SPACE] button  mus be press.
- To bend the avatar the [CONTROL] button must to be pressed.
- To see the avatar dance the [F] button must to be pressed.
- To see the animation of interact the [E] button must to be pressed.
- To aim the [RIGHTCLICK] of the mouse must to be pressed.
- To shoot first it's need to aim an then press the [LEFTCLICK] of the mouse must to be pressed.

## To play with the free carmera:

- It's needs to charge the Test-R1.1.2 scene.
- Here the player got  some obtacles to jump an interact to test the movement of the avatar.
- The movement can be done with the button [W,A,S,D] and the arrows buttons[UP,DOWN,LEFT,RIGHT].
- To jump the [SPACE] button  mus be press.
- To bend the avatar the [CONTROL] button must to be pressed.
- To see the avatar dance the [F] button must to be pressed.
- To see the animation of interact the [E] button must to be pressed.
- To aim the [RIGHTCLICK] of the mouse must to be pressed.
- To shoot first it's need to aim an then press the [LEFTCLICK] of the mouse must to be pressed.
