using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;


public enum PlayerStates
{
    Idle,
    Walking,
    Interacting,
    Aiming,
    Dancing
}

public class InputSystemController : MonoBehaviour
{
    #region Variables: Movement

    private CharacterController _characterController;
    private Vector3 _direction;
    private float _walkingSpeed = 3f;
    private float _runningSpeed = 6f;
    private CamDirection _cameraDirection;
    //To test
    private Camera _playerCamera;
    #endregion
    #region Variables: Gravity
    [SerializeField] private float _gravity = -9.8f;
    private float _fallVelocity;
    #endregion
    #region Variables: Jumping
    private float jumpPower = 5f;
    #endregion
    #region Variables: Animator

    [SerializeField] private Animator _animator;

    #endregion
    #region Variables: Inputs
    public Vector2 _move;
    public bool _aim;
    public bool _jump;
    public Vector2 _look;
    public bool _run;
    public bool _bend;
    public bool _dance;
    public bool _fire;
    public bool _action;
    #endregion
    public float turnSmoothTime = 0.1f;
    private float _turnSmoothVelocity;
    Vector3 moveDir;
    public PlayerStates _movementStates;
    #region Variables: Cinemachine
    public GameObject CinemachineCameraTarget;
    public float TopClamp = 70.0f;
    public float BottomClamp = -30.0f;
    public float CameraAngleOverride = 0.0f;
    private float _cinemachineTargetYaw;
    private float _cinemachineTargetPitch;
    public float Sensitivity = 30f;
    private const float _threshold = 0.01f;
    #endregion
    private void Awake()
    {
        _characterController = GameObject.Find("Player").GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
        _cameraDirection = GetComponent<CamDirection>();
        _movementStates = PlayerStates.Idle;
        _playerCamera = Camera.main;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {

        ApplyAnimations();
        Move();
    }

    private void ApplyGravity()
    {
        if (_characterController.isGrounded)
        {
            _fallVelocity = _gravity * Time.deltaTime;
            _direction.y = _fallVelocity;
        }
        else
        {
            _fallVelocity += _gravity * Time.deltaTime;
            _direction.y = _fallVelocity;
        }
    }
    void Move()
    {
        _direction = new Vector3(_move.x, 0, _move.y).normalized;

        float speed = (_run ? _runningSpeed : _walkingSpeed);

        if (_direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(_direction.x, _direction.z) * Mathf.Rad2Deg + _playerCamera.transform.eulerAngles.y;

            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref _turnSmoothVelocity, turnSmoothTime);
            if (!_aim)
                transform.rotation = Quaternion.Euler(0f, angle, 0f);
            ApplyGravity();
            Jump();
            moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            _characterController.Move(new Vector3(moveDir.normalized.x, _direction.normalized.y, moveDir.normalized.z) * speed * Time.deltaTime);
        }
        else
        {
            ApplyGravity();
            Jump();
            _characterController.Move(new Vector3(0f, _direction.normalized.y, 0f) * speed * Time.deltaTime);
        }


    }


    public void Jump()
    {
        if (_jump && IsGrounded())
        {
            _fallVelocity = jumpPower;
            _direction.y = _fallVelocity;
            _animator.SetTrigger("Jumping");

        }
        // _animator.ResetTrigger("Jumping");

    }
    private void ApplyAnimations()
    {
        // JumpAnimation();
        MoveAnimation();
        RunAnimation();
        BendAnimation();
        DanceAnimation();
        AimAnimation();
        ActionAnimation();
    }

    private void LateUpdate()
    {
        CameraRotation();
    }
    private void CameraRotation()
    {
        // if there is an input
        if (_look.sqrMagnitude >= _threshold)
        {
            _cinemachineTargetYaw += _look.x * Time.deltaTime * Sensitivity;
            _cinemachineTargetPitch += _look.y * Time.deltaTime * Sensitivity;
        }

        // clamp our rotations so our values are limited 360 degrees
        _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
        _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

        // Cinemachine will follow this target
        CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + -CameraAngleOverride, _cinemachineTargetYaw, 0.0f);
    }
    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }
    public void OnMove(InputAction.CallbackContext context)
    {
        _move = context.ReadValue<Vector2>();
        _movementStates = PlayerStates.Walking;

    }
    public void OnRun(InputAction.CallbackContext context)
    {
        _run = context.ReadValueAsButton();

    }
    public void OnJump(InputAction.CallbackContext context)
    {
        _jump = context.ReadValueAsButton();
        //_animator.SetTrigger("Jumping");

    }
    public void OnDance(InputAction.CallbackContext context)
    {
        _dance = context.ReadValueAsButton();
        _movementStates = PlayerStates.Dancing;

    }
    public void OnBend(InputAction.CallbackContext context)
    {
        _bend = context.ReadValueAsButton();

    }
    public void OnLook(InputAction.CallbackContext context)
    {

        _look = context.ReadValue<Vector2>();

    }
    public void OnFire(InputAction.CallbackContext context)
    {
        _fire = context.ReadValueAsButton();
    }
    public void OnAim(InputAction.CallbackContext context)
    {
        _aim = context.ReadValueAsButton();
        _movementStates = PlayerStates.Aiming;
    }
    public void OnAction(InputAction.CallbackContext context)
    {
        if (Keyboard.current.eKey.wasPressedThisFrame)
        {
            _action = context.ReadValueAsButton();
            _movementStates = PlayerStates.Interacting;
        }
        else
        {
            _action = false;
        }
    }

    private IEnumerator WaitForCrouching()
    {
        yield return new WaitForSeconds(_animator.GetAnimatorTransitionInfo(0).duration);
        _animator.SetBool("Standing", true);

    }
    private bool IsGrounded() => _characterController.isGrounded;
    private void MoveAnimation()
    {
        if (_move.x != 0 || _move.y != 0)
        {
            _animator.SetBool("Walking", true);
            _movementStates = PlayerStates.Walking;
        }
        else
        {
            if (_movementStates == PlayerStates.Dancing)
                _movementStates = PlayerStates.Dancing;
            else
                _movementStates = PlayerStates.Idle;

            _animator.SetBool("Walking", false);

        }

    }
    private void RunAnimation()
    {
        if (_run)
        {
            _animator.SetBool("Running", true);
        }
        else
        {
            _animator.SetBool("Running", false);
        }
    }
    private void BendAnimation()
    {
        if (_bend)
        {
            _animator.SetBool("Standing", false);

        }
        else //This is for preventing pressing the control button and realease it before the animation of crouching is finished
        {
            StartCoroutine(WaitForCrouching());
        }
    }
    private void DanceAnimation()
    {
        if (_dance)
        {
            _animator.SetTrigger("Dance");
            _movementStates = PlayerStates.Dancing;
            StartCoroutine(WaitForDance());
        }

    }
    private IEnumerator WaitForDance()
    {
        yield return new WaitForSeconds(2f);//Wait for the transition
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);
        _animator.SetBool("Dance", false);
        _movementStates = PlayerStates.Idle;
    }
    private void AimAnimation()
    {

        if (_aim)
        {

            _animator.SetLayerWeight(1, 1);
        }
        else
        {
            _animator.SetLayerWeight(1, 0);
        }


    }
    private void ActionAnimation()
    {

        if (_action)
        {
            _animator.SetTrigger("Interact");
        }

    }

}
