using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _aimVirtualCamera;
    [SerializeField] private CinemachineVirtualCamera _danceVirtualCamera;

    private InputSystemController inputSystemController;
    private void Awake()
    {
        inputSystemController = GameObject.Find("PlayerModel").GetComponent<InputSystemController>();
    }


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (inputSystemController._aim)
        {
            _aimVirtualCamera.gameObject.SetActive(true);

        }
        else
        {
            _aimVirtualCamera.gameObject.SetActive(false);
        }

        if (inputSystemController._movementStates == PlayerStates.Dancing)
        {
            _danceVirtualCamera.gameObject.SetActive(true);

        }
        else
        {
            _danceVirtualCamera.gameObject.SetActive(false);
        }

    }
}
